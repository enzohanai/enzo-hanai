import React from 'react';
import {View, Text, TextInput, StyleSheet, Alert, TouchableOpacity,ScrollView, AsyncStorage} from 'react-native';


export default class Register extends React.Component{
    constructor(props) {
      super(props);
      this.state = {
        login: '',
        senha: '',
      };
    }

  checkData(){
    if ((this.state.login) && (this.state.senha) != ''){
      this.props.navigation.push('Login',{
        login: this.state.login,
        senha: this.state.senha
      }),
      
      // this.props.navigation.navigate('DB',{
      //   login: this.state.login,
      //   senha: this.state.senha
      // });
      
      Alert.alert("Cadastro concluido com sucesso!")
      // Alert.alert(this.state.login)
      // Alert.alert(this.state.senha);
    }

    else{
      Alert.alert("Cadastro inválido!")
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.userInput}>
            <TextInput
                placeholder="Login"
                onChangeText={(login) => this.setState({login})}
            />
          </View>

          <View style={styles.userInput}>
            <TextInput
                placeholder="Senha"
                onChangeText={(senha) => this.setState({senha})}
            />
          </View>
          
          <TouchableOpacity style={styles.sendButton} 
            onPress={() => this.checkData()}>
                <View>
                    <Text style={{fontSize: 18, color: 'white'}}>Cadastrar</Text>
                </View>
            </TouchableOpacity>
        </ScrollView>  
      </View>
    );
  };
};



const styles = StyleSheet.create({

  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
},
  userInput: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    height: 50,
    width: 300,
    backgroundColor: 'rgba(0,0,0,0.02)'

  },
  sendButton: {
    // flex: 1,
    margin: 30,
    left: 50,
    height: 50,
    width: 175,
    justifyContent: 'center', 
    alignItems: 'center',
    backgroundColor: 'blue'
  },

})