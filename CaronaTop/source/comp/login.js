import React from 'react';
import { Button, View, Text, TextInput, StyleSheet, Alert, TouchableOpacity} from 'react-native';


export default class Login extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            Login: '',
            Senha: ''
        };
      }
    
    setLogin(l) {
        this.state.Login = l;
    }

    setSenha(s) {
        this.state.Senha = s;
    }

    checkData(){
        if ((this.state.Login) == (this.props.navigation.getParam('login')) && (this.state.Senha) == (this.props.navigation.getParam('senha'))){
        Alert.alert("Login efutuado com sucesso!", "Boas viagens!");
        this.props.navigation.navigate('User');
        }

        else{
        Alert.alert("Login ou senha inválidos!")
        // alert("Login: "+ this.state.Login),
        // alert("Senha: "+ this.state.Senha),
        // alert(this.props.navigation.getParam('login')),
        // alert(this.props.navigation.getParam('senha'))
        }
    }

    render() {
      return (
        <View style={styles.container}>
          <View style={styles.userInput}>
            <TextInput
                placeholder="Login"
                onChangeText={(login) => this.setLogin(login)}
            />
          </View>

          <View style={styles.userInput}>
            <TextInput
                placeholder="Senha"
                onChangeText={(senha) => this.setSenha(senha)}
            />
          </View>
          
          <TouchableOpacity style={styles.loginButton} 
            onPress={() => this.checkData()}>
                <View>
                    <Text style={{fontSize: 18, color: 'white'}}>Entrar</Text>
                </View>
            </TouchableOpacity>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    userInput: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        margin: 20,
        height: 50,
        width: 300,
        backgroundColor: 'rgba(0,0,0,0.02)'
    },
      loginButton: {
        margin: 30,
        height: 50,
        width: 175,
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: 'orange'
    },
  })