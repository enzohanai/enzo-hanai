import React from 'react';
import { Button, View, Text, StyleSheet, TouchableHighlight, TouchableOpacity, Image } from 'react-native';

export default class Home extends React.Component{
  render() {
    return (
      <View style={styles.container}>
            <View style={styles.txt}>
                <Text style={{fontSize: 24, color: "black"}}>Bem vindo ao</Text>
                <Text style={{fontSize: 24, color: "black"}}>CaronaTop!</Text>
            </View>
            
            <TouchableOpacity style={styles.loginButton} onPress={() => this.props.navigation.navigate('Login')}>
                <View>
                    <Text style={{fontSize: 18, color: 'black'}}>Entrar</Text>
                </View>
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.registerButton} onPress={() => this.props.navigation.navigate('Register')}>
                <View>
                    <Text style={{fontSize: 18, color: 'black'}}>Cadastrar</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.0)'
    },
    txt: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center' 
    },
    registerButton: {
        // flex: 1,
        margin: 30,
        height: 50,
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: 'rgba(255,140,0,0.5)'
    },
    loginButton: {
        // flex: 1,
        margin: 30,
        height: 50,
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: 'rgba(0,255,0,0.5)'    
    },

})